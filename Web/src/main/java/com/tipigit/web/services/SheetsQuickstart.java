package com.tipigit.web.services;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.tipigit.core.DAO.ProjectRepository;
import com.tipigit.core.entities.Sprint;
import com.tipigit.core.entities.Task;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;



public class SheetsQuickstart {
	
	private static final SheetsQuickstart SHEETS_QUICKSTART = new SheetsQuickstart();
	private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	
	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";
	String[] tempArray;

	private SheetsQuickstart() {

	}

	public static SheetsQuickstart getInstance() {
		return SHEETS_QUICKSTART;
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = SheetsQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	/********
	 * reading sprints start date and deadline
	 * 
	 * @param index         of the sprint
	 * @param spreadsheetId and service to invoke the google service reading only
	 *                      method
	 * @throws IOException 
	 */
	
	
		public List<Task> ReadTaskForSprint(int index,Sheets service,String spreadsheetId) throws IOException
		{       List<Task> tasks=new ArrayList<Task>();

			final String range = "'Product Backlog'!B3:H";
			ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
			List<List<Object>> values = response.getValues();

			if (values == null || values.isEmpty()) {
				System.out.println("No data found.");
			} else {
				System.out.println("task, estimation period");
				for (List row : values) {
					// Print columns A and E, which correspond to indices 0 and 4.

					System.out.printf("%s \t %s %s  \n", row.get(0), row.get(2), row.get(4));
					if (Integer.parseInt((String) row.get(4)) ==index)
						tasks.add(new Task((String)row.get(0),"",Integer.parseInt((String) row.get(2))));
				}
				}
return tasks;
			
		}
		
		
		
	
	public void ReadSprintSheet(int index, Sheets service, String spreadsheetId) throws IOException {
		String rangeSprint = "Sprint " + Integer.toString(index) + "!B3:D";

		ValueRange response = service.spreadsheets().values().get(spreadsheetId, rangeSprint).execute();

		System.out.println("ererere" + service.getApplicationName());
		List<List<Object>> values = response.getValues();
		if (values == null || values.isEmpty()) {
			System.out.println("No data found.");
		} else {
			System.out.println("Start, deadline");

			// Print columns B and C, which correspond to indices 0 and 1.

			System.out.printf("%s \t %s \n", values.get(0), values.get(1));

			tempArray = (values.get(1).toString().replace("[", "").replace("]", "").replace(" ", "")).split(",");

		}
	}

	/**
	 * Initializing and reading from the backlog googleSheet
	 * 
	 * @param spreadsheetId
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public List<Sprint> init(String spreadsheetId) throws IOException, GeneralSecurityException {
		
		// Build a new authorized API client service.
		int maxSprint = 0;
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		// final String spreadsheetId = spreedID;
		// "1OPXXNep-AEtA6tJ_t4XMgcoKHnsNln7_DwuuEaljGOo";

		final String range = "'Product Backlog'!B3:G";
		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();

		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();

		List<List<Object>> values = response.getValues();
		if (values == null || values.isEmpty()) {
			System.out.println("No data found.");
		} else {
			System.out.println("task, estimation period");
			for (List row : values) {
				// Print columns A and E, which correspond to indices 0 and 4.

				System.out.printf("%s \t %s %s  \n", row.get(0), row.get(2), row.get(4));
				if (Integer.parseInt((String) row.get(4)) > maxSprint)
					maxSprint = Integer.parseInt((String) row.get(4));

			}

		}
		System.out.print("max srpint is " + maxSprint);
		List<Sprint> sprintlist = new ArrayList<Sprint>();
       List<Task> tasks=new ArrayList<Task>();
		for (int i = 1; i <= maxSprint; i++) {
			Sprint s = new Sprint();

			System.out.print("reading sprint" + i + "\\n");
			/**
			 * caling the methode to output the reading section of every sprint
			 */
			System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
			ReadSprintSheet(i, service, spreadsheetId);
            
			System.out.println(tempArray[0]);
			s.setSprintName("Sprint" + i);
			s.setStartingDate(tempArray[0]);
			//s.setEndDate(tempArray[1]);
			s.setDayNumber(Integer.parseInt(tempArray[2]));
			tasks=ReadTaskForSprint(i, service, spreadsheetId);
			s.setTasks(tasks);
			for(int j=0;j<tasks.size();j++)
			tasks.get(j).setSprint(s);
			System.out.println("hello looooool");
			System.out.println(s.getDayNumber());
			System.out.println(s.getEndDate());

			sprintlist.add(s);
		}

		return (sprintlist);
	}
}
