package com.tipigit.web.services;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.tipigit.core.DAO.AuthorRepository;
import com.tipigit.core.DAO.NotificationRepository;
import com.tipigit.core.entities.Notifications;
@Service
public class TimeScheduling {
	
	@Autowired
	private NotificationRepository notificationRepository;
	public List<Notifications> demoServiceMethod(SimpMessagingTemplate template, List<Notifications> notificationsTotal) 
	{
		System.out.println("Method executed at every 1 hour between 9 and 5pm. Current time is :: " + new Date());
		Notifications notifications1=new Notifications();
		Notifications notifications2=new Notifications();

		notifications1.setUsername("chaima");
		notifications1.setIdTask(Long.valueOf(1325));
		notifications1.setLeftHours(8);
		notifications1.decriment();
		notifications2.setUsername("Hajer");
		notifications2.setIdTask(Long.valueOf(13244));
		notifications2.setLeftHours(18);
		notificationRepository.save(notifications1);
		notificationRepository.save(notifications2);
		notificationsTotal.add(notifications1);
		notificationsTotal.add(notifications2);
	
		
	
		         //Increment Notification by one
		       
		       
		       
		      // notifications.setTaskIdStarted(l );
		
		       
		
	
		        System.out.println(template.toString());

		         //Push notifications to front-end
		        template.convertAndSend("/topic/notification", notificationsTotal);
		        return notificationsTotal;
		
	}
}
