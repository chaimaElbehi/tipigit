package com.tipigit.web;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.web.bind.annotation.RequestMapping;


import org.springframework.scheduling.annotation.EnableScheduling;
@EntityScan(basePackages = "com.tipigit.core.entities")
@EnableJpaRepositories(basePackages = "com.tipigit.core.DAO")
@SpringBootApplication
@EnableScheduling
public class DemoApplication {
	
	@RequestMapping("/")
	public String greeting() {
		return "commits";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		
		
		
	}



}
