package com.tipigit.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tipigit.core.DAO.SprintRepository;
import com.tipigit.core.entities.Sprint;
import com.tipigit.core.entities.Task;

@RestController
@CrossOrigin("*")
public class SprintController {
	@Autowired
	SprintRepository sprintRepository;

	@GetMapping(value = "/findSprintByProject")
	public List<Sprint> saveSprint(@RequestParam String nameProject) {
		return (sprintRepository.findSprintByProject(nameProject));

	}

	@GetMapping(value = "/findSprintByProjectandSprintNumber")

	public List<Task> findtaskBySprintandProject(@RequestParam String nameProject, @RequestParam String nameSprint) {
		return sprintRepository.findtaskBySprintAndProjectName(nameSprint, nameProject);
	}

}
