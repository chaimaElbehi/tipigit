package com.tipigit.web.controller;

import java.time.LocalTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.persistence.Access;
import javax.validation.Valid;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter.SseEventBuilder;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.tipigit.core.DAO.AuthorRepository;
import com.tipigit.core.DAO.TeamRepository;
import com.tipigit.core.DTO.AuthorDTO;
import com.tipigit.core.entities.Author;
import com.tipigit.core.entities.Project;
import com.tipigit.core.entities.Team;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/author")

public class AuthorController {
	@Autowired
	AuthorRepository authorRepository;
	@Autowired
	TeamRepository teamRepository;

	@GetMapping(value = "/findAuthorByName")
	public Author findAuthorByName(@RequestParam String name) {
		return authorRepository.findAuthorByName(name);

	}

	@GetMapping(value = "/countDev")
	public long count() {
		return authorRepository.count();
	}

	@GetMapping(value = "/authorsName")
	public List<String> findAuthorsName() {
		return authorRepository.findAuthorsName();
	}

	@GetMapping(value = "/listAuthor")
	public List<Author> findauthors() {
		return authorRepository.findAll();
	}

	@GetMapping(value = "/searchAuthor")
	public Page<Author> searchAuthor(@RequestParam(name = "mc", defaultValue = "") String mc,
			@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "3") int size) {
		Pageable pageable = PageRequest.of(page, size);
		return authorRepository.search("%" + mc + "%", pageable);
	}
	
	@GetMapping(value="/getCommitsAuthors")
	public AuthorDTO getComments(@RequestParam Long id)
	{
		Author auth=authorRepository.findAuthorById(id);
		AuthorDTO authDTO=new AuthorDTO();
		authDTO.setAuthorProjects(auth.getAuthorProjects());
		authDTO.setCommits(auth.getCommits());
		authDTO.setEmail(auth.getEmail());
		authDTO.setName(auth.getName());
		authDTO.setRole(auth.getRole());
		authDTO.setIdAuthor(auth.getIdAuthor());
		authDTO.setTeam(auth.getTeam());
		return authDTO;
	
	}
	@GetMapping(value="/userRole")
	public HashMap<String,String> getUserRole(@RequestParam String username)
	{
		
	      HashMap<String,String> hmap = new HashMap<String, String>();
	      
	      hmap.put("role", authorRepository.getRoleUser(username));
	      return hmap;

	}


		
		
	
@GetMapping(value="/delete")
public String deleteAuthor(@RequestParam Long idAuthor)
{
	authorRepository.deleteById(idAuthor);
	return "yes";}
	

	@PostMapping(value = "/addAuthor")
	public Author addAuthor(@RequestParam String name, @RequestParam String email, @RequestParam String role) {
		return authorRepository.save(new Author(name, email, role));
	}

	
	
	
	@PutMapping(value = "/updateAdmin")
	public Author  updateAdmin(@RequestParam Long id, @RequestBody Author author//,@RequestParam Long idteam
			) {

		Author currentAuthor = authorRepository.findAuthorById(id);
        // Team team=teamRepository.findTeamById(idteam);
         //System.out.println(team.getNameLeader());
         
		if (currentAuthor == null) {
			return(currentAuthor);
			
		}
		//gggoijg,olijogorijtoigj
		System.out.print("ffffff");
      // currentAuthor.setTeam(team);
		currentAuthor.setEmail(author.getEmail());
		currentAuthor.setName(author.getName());
		currentAuthor.setRole(author.getRole());
	currentAuthor.setUsername(author.getUsername());
	currentAuthor.setPassword(author.getPassword());
		authorRepository.save(currentAuthor);

		return (currentAuthor);

	}

	@PutMapping(value = "/updateAdminWithTeam")
	public String  updateAdmin(@RequestParam Long id, @RequestBody Author author,@RequestParam Long idteam
			) {

		Author currentAuthor = authorRepository.findAuthorById(id);
         Team team=teamRepository.findTeamById(idteam);
         System.out.println(team.getNameLeader());
         
		if (currentAuthor == null) {
			return("Unable to update. Admin with id {} not found.");
			
		}
       currentAuthor.setTeam(team);
		currentAuthor.setEmail(author.getEmail());
		currentAuthor.setName(author.getName());
		currentAuthor.setRole(author.getRole());
	
		authorRepository.save(currentAuthor);

		return ("success");

	}
	@GetMapping(value="/getUserbyusername")
	public Author getAuthorByusername(@RequestParam String username)
	{
		return authorRepository.getAuthorByusername(username);
	}
@GetMapping(value="/getUsersByUsername")

	public List<String> ListUsername()
	{
	return authorRepository.Getusernames();	}
	@GetMapping(value="/getAuthorsTeam")
	
		public List<Author> ListAUthorsTeam(@RequestParam Long idteam)
		{
			return authorRepository.getAllAuthorsTeam(idteam);
		}
	@GetMapping (value="/getallauthorsName")
	List <String> getAllAuthorsName()
	
	{
		return authorRepository.getAuthorsName();
	}
	@GetMapping(value="/loginAuthReq")
	Boolean auth(@RequestParam String username,@RequestParam String password) {
		if(authorRepository.authentification(username, password)==null)
			return false; 
			else 
				return true;
	}
	
	@GetMapping("/stream-sse-mvc")
	public SseEmitter streamSseMvc() {
	    SseEmitter emitter = new SseEmitter();
	    ExecutorService sseMvcExecutor = Executors.newSingleThreadExecutor();
	    sseMvcExecutor.execute(() -> {
	        try {
	            for (int i = 0; true; i++) {
	                SseEventBuilder event = SseEmitter.event()
	                  .data("SSE MVC - " + LocalTime.now().toString())
	                  .id(String.valueOf(i))
	                  .name("sse event - mvc");
	                emitter.send(event);
	                Thread.sleep(1000);
	            }
	        } catch (Exception ex) {
	            emitter.completeWithError(ex);
	        }
	    });
	    return emitter;
	}
	
}
