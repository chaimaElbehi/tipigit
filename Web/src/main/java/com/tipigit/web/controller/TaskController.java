package com.tipigit.web.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tipigit.core.DAO.AuthorRepository;
import com.tipigit.core.DAO.CommitRepository;
import com.tipigit.core.DAO.TaskNotificationRepository;
import com.tipigit.core.DAO.TaskRepository;
import com.tipigit.core.entities.Author;
import com.tipigit.core.entities.Task;
import com.tipigit.core.entities.TaskNotification;
import com.tipigit.core.entities.Team;
import com.tipigit.web.services.BonusPointCalcul;

@RestController
@CrossOrigin("*")
public class TaskController {
	@Autowired
	TaskRepository taskRepository;
	@Autowired
	TaskNotificationRepository taskNotificationRepository;
	@Autowired
	AuthorRepository authorRepository;
	@Autowired
	BonusPointCalcul bonusPointCalcul;
	@Autowired 
	CommitRepository commitRepository;

	@GetMapping(value = "/ListTaskBySprintId")
	List<Task> ListTaskBySprintId(@RequestParam Long idsprint) {
		return taskRepository.ListTaskBySprintId(idsprint);
	}

	@GetMapping(value = "/ListTaskByAuthorName")
	List<Task> FindTaskByauthorName(@RequestParam String authorName) {
		return taskRepository.FindTaskByauthorName(authorName);
	}

	@GetMapping(value = "/countNotFinishedTasks")
	HashMap<String, Integer> countNotFinishedTasks(@RequestParam String name) {
		HashMap<String, Integer> result = new HashMap<>();
		result.put("number", taskRepository.countNotFinishedTasks(name));
		return result;

	}

	@GetMapping(value = "/countFinishedTasks")
	HashMap<String, Integer> countFinishedTasks(@RequestParam String name) {

		HashMap<String, Integer> result = new HashMap<>();
		result.put("finishedTasks", taskRepository.countFinishedTasks(name));
		result.put("NotStartedNotFinished", taskRepository.countNotStartNotFinishedTasks(name));

		return result;

	}

	@GetMapping(value = "/countStateTasks")
	HashMap<String, Integer> countTasks(@RequestParam String name) {

		HashMap<String, Integer> result = new HashMap<>();
		result.put("finishedTasks", taskRepository.countFinishedTasks(name));
		result.put("NotStartedNotFinished", taskRepository.countNotStartNotFinishedTasks(name));
		result.put("StartedNotFinished", taskRepository.countNotFinishedTasks(name));
		return result;

	}

	@GetMapping(value = "/countNotFinishedNotStartedTasks")
	HashMap<String, Integer> countNotFinishedNotStartedTasks(@RequestParam String name) {
		HashMap<String, Integer> result = new HashMap<>();
		result.put("number", taskRepository.countNotStartNotFinishedTasks(name));
		return result;
	}

//	@GetMapping(value="/update")
//	void update(@RequestParam Long id)
//	{
//		 taskRepository.ReduceLeftHours(id);
//	}

	/*
	 * @GetMapping(value="/listTaskByAuthorId") public List<Task>
	 * findTakIDAuthor(@RequestParam Long idAuthor) { return
	 * taskRepository.findTasksByauthorId(idAuthor);}
	 */
	@GetMapping(value = "/taskinProgressbyUsername")
	List<Task> getProgressTakByUsername(@RequestParam String username) {
		return taskRepository.tasksInProgressByUsername(username);
	}

	@GetMapping(value = "/taskTodobyUsername")
	List<Task> taskTodobyUsername(@RequestParam String username) {
		return taskRepository.taskstoDoByUsername(username);
	}

	@GetMapping(value = "/taskinDonebyUsername")
	List<Task> getDoneTakByUsername(@RequestParam String username) {
		return taskRepository.tasksDoneByUsername(username);
	}

	@PutMapping(value = "/updateTaskAUthor")
	public String updateAdmin(@RequestParam Long id, @RequestParam String nameAuthor) {

		Task currentTask = taskRepository.findTaskById(id);

		if (currentTask == null) {
			return ("Unable to update. task with id {} not found.");

		}

		currentTask.setAuthorName(nameAuthor);

		taskRepository.save(currentTask);
		// add notificaiton to the table task////////////
		TaskNotification taskNotification = new TaskNotification();
		taskNotification.setDateTimestamp(Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
		taskNotification.setMessage("Task has been added to you ");
		String username = authorRepository.GetusernameFromAuthorName(currentTask.getAuthorName());
		taskNotification.setUsername(username);
		taskNotificationRepository.save(taskNotification);

		return ("success");

	}

	@GetMapping(value = "/getNotification")
	public List<TaskNotification> getTaskNotification(@RequestParam String username) {
		return taskNotificationRepository.GetusernameFromAuthorName(username);
	}

	@GetMapping(value = "/notificationCountUser")
	public Integer getNotificationNumber(@RequestParam String username) {
		return taskNotificationRepository.countNbrNotificationPerUsername(username);
	}

	@GetMapping(value = "/timeStamp")
	public int Calcul() throws ParseException {
		return bonusPointCalcul.calculBonusPointAccumulated("22/02/2019","25/02/2019");

//		String dateStart =commitRepository.getOne(Long.valueOf(1159)).getTimestamp();
//		return bonusPointCalcul.calculBonusPointAccumulated(taskRepository.getOne(Long.valueOf(1325)).getStartingDate(),
//				taskRepository.getOne(Long.valueOf(1325)).getEndingDate());

		

	}
}
