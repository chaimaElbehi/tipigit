package com.tipigit.web.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tipigit.core.DAO.CommitRepository;
import com.tipigit.core.DAO.TaskRepository;
import com.tipigit.core.entities.Commit;
import com.tipigit.core.entities.Task;
import com.tipigit.core.model.CommitInfoModel;


@RestController
@CrossOrigin("*")

@RequestMapping(value = "/commit")
public class CommitController {
	@Autowired
	CommitRepository commitRepository;
	@Autowired
	TaskRepository taskRepository;

	@GetMapping(value = "/getCommitsByAuthorName")
	public List<String> findMessageCommit(@RequestParam String authorName) {
		return commitRepository.findCommitMessageByAuthor(authorName);

	}

	@GetMapping(value = "/test")
	public void addTask() {
		Commit c = commitRepository.getOne((long) 3);
		Task t = taskRepository.getOne((long) 256);
		c.setTask(t);
		commitRepository.save(c);

	}

	@GetMapping(value = "/all")
	List<Object> all() {

		return commitRepository.findInfoNotification();
	}

	@GetMapping(value = "/getAllCommitsInfoByUser")
	public List<Commit>findallCommitsInfoByUserName(@RequestParam String authorName) {
	
		
		return commitRepository.findallCommitsInfoByUserName(authorName);
	}
}