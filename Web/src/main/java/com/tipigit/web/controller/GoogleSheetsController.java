package com.tipigit.web.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tipigit.core.DAO.ProjectRepository;
import com.tipigit.core.DAO.SprintRepository;
import com.tipigit.core.DAO.TaskRepository;
import com.tipigit.core.entities.Sprint;
import com.tipigit.core.entities.Task;
import com.tipigit.core.model.HashFunctionForTasks;
import com.tipigit.web.services.SheetsQuickstart;

@RestController
public class GoogleSheetsController {
	@Autowired
	SprintRepository sprintRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired 
	TaskRepository taskRepository;
	Sprint sprint = new Sprint();

	/**
	 * ask the user to enter the spreadsheet ID
	 * 
	 * @param sheetID
	 * @throws IOException
	 * @throws GeneralSecurityException
	 * @throws ParseException 
	 */
	@GetMapping(value = "/googlesheets")
	/*
	 * enter the sheetBase Url and here I will calculate the ending date of each task
	 */
	public void googlesheets(@RequestParam String sheetID, @RequestParam String projectName)
			throws IOException, GeneralSecurityException, ParseException {
		List<Sprint> sprintlist = new ArrayList<Sprint>();
List<Task>tasks=new ArrayList<Task>();
		sprintlist = SheetsQuickstart.getInstance().init(sheetID);
		
		for (int i = 0; i < sprintlist.size(); i++)
			{sprintlist.get(i).setProject(projectRepository.findProjectByname(projectName));
			tasks=sprintlist.get(i).getTasks();
			for (int j=0;j<tasks.size();j++)
	        {tasks.get(j).setStartingDate(sprintlist.get(i).getStartingDate());
	       
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			 Date date = sdf. parse(tasks.get(j).getStartingDate());
			 Calendar cal = Calendar. getInstance();
			 cal. setTime(date);
				System.out.println(date);
			 Date endDate=getTaskEndTime(cal,getWorkingHours(tasks.get(j).getEstimationDuration()));
			 //System.out.print(endDate);
			 SimpleDateFormat sdf1=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
			 String strEndDate = sdf1.format(endDate); 
			 System.out.print(strEndDate);
			 tasks.get(j).setEndingDate(strEndDate);
			 tasks.get(j).setKeyTask(UUID.randomUUID().toString());
			 tasks.get(j).setLefthours(tasks.get(j).getEstimationDuration()/2);
	        }
			taskRepository.saveAll(tasks);
			
			
			
			}
			
		sprintRepository.saveAll(sprintlist);
	}
/*******function for getting tasks end time ****/
	public Date getTaskEndTime(Calendar startDate, int hours){
	    while (hours > 0){
	        int step = 0;
	        if(hours > 24) step = 24;
	        else step = hours;          
	        hours -= step;          
	        startDate.add(Calendar.HOUR_OF_DAY, step);          
	        int dayOfWeek = startDate.get(Calendar.DAY_OF_WEEK);
	        if(dayOfWeek == Calendar.SATURDAY) hours += 24;
	        if(dayOfWeek == Calendar.SUNDAY) hours += 24;
	    }
	    return startDate.getTime();
	}
	/******getting tasks end time***/
	
	public int getWorkingHours(int a)
	{
      return ((a/2)/8)*24+(a/2)%8+9;		
	
	}
	
}
