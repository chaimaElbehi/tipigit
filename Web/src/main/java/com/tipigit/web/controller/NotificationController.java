package com.tipigit.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tipigit.core.DAO.NotificationRepository;
import com.tipigit.core.DAO.TaskRepository;
import com.tipigit.core.entities.Notifications;
import com.tipigit.core.entities.Task;
import com.tipigit.web.services.ExperienceService;
import com.tipigit.web.services.TaskService;
import com.tipigit.web.services.TimeScheduling;
@RestController
@CrossOrigin("*")

public class NotificationController {

    @Autowired 
    private SimpMessagingTemplate template;
    @Autowired 
    private TimeScheduling timeService;
    @Autowired
    private ExperienceService exp;
    
    //private static final String CRON_SCHEDULE = "0 0 9,10,11,12,14,15,16,17,18 ? * MON-FRI *";
    //private static final String USER_TIMEZONE = "";
    // Initialize Notifications
    @Autowired
    private TaskRepository taskRepository;
	public List <Notifications> notifications = new ArrayList<Notifications>();

	
    
	@PostConstruct
	  public void init(){
	     // init code goes here
		
		//notifications.setCount((taskRepository.findEstimationBytaskId(Long.valueOf(1325))/2));
		
	
		
	
	  }
 @GetMapping(value="/test")
 public String getLevel()
 {
	 return exp.ExperinceToLevel(223.0f);
	 
 }
 
 
    @GetMapping(value= "/notify")
    @Scheduled(cron = "0 0 9,10,11,12,14,15,16,17,18 ? * MON-FRI")
    //@Scheduled(fixedRate=5000)
    public String getNotification() {
		

    	notifications=timeService.demoServiceMethod(template,notifications);
    	taskRepository.ReduceLeftHours(Long.valueOf(1325));

       template.convertAndSend("/topic/notification", notifications);
      
  	 



        return "Notifications successfully sent to Angular !";
    }
}