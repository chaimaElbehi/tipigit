package com.tipigit.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tipigit.core.DAO.AuthorProjectRepository;
import com.tipigit.core.DAO.AuthorRepository;
import com.tipigit.core.DAO.CommitRepository;
import com.tipigit.core.DAO.ProjectRepository;
import com.tipigit.core.DAO.TaskRepository;
import com.tipigit.core.entities.Author;
import com.tipigit.core.entities.AuthorProject;
import com.tipigit.core.entities.AuthorProjectPK;
import com.tipigit.core.entities.Commit;
import com.tipigit.core.entities.Project;
import com.tipigit.core.entities.Task;
import com.tipigit.core.model.WebhookResp;
import com.tipigit.web.services.BonusPointCalcul;
import com.tipigit.web.services.TimeScheduling;

@RestController
public class ContactRestService {
	/**** REPOSITORY INSTANTIATION ***************************/
	@Autowired
	AuthorRepository authorRepository;
	@Autowired
	CommitRepository commitRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	AuthorProjectRepository authorProjectRepository;
	@Autowired
	TaskRepository taskRepository;
	@Autowired
	BonusPointCalcul bonusPointCalcul;

	/*****************************
	 * get data from Webhook
	 * @throws IOException 
	 *****************************************/
	@PostMapping(value = "/commits")

	// consumes = "application/json"

	public void process(@RequestBody String payload,HttpServletResponse response) throws IOException {
		/*********** Local variables *************/
		Boolean taskEnd = false;
		Boolean taskStart = false;
		Gson gson = new GsonBuilder().create();
		Author author = new Author();
		Commit commit = new Commit();
		Project project = new Project();
		AuthorProject authorProject = new AuthorProject();

		AuthorProjectPK authorProjectPK = new AuthorProjectPK();

		/*
		 * authorProjectPK.setIdAuthor(10L); authorProjectPK.setIdProject(15L);
		 * authorProjectPK.setCreationDate(null);
		 * 
		 * authorProject.setAuthorProjectPK(authorProjectPK);
		 * authorProject.setAuthor(author); authorProject.setProject(project);
		 */
		// commit.setAuthor(author);

		/************ Extraction of data from Json *************************/
		WebhookResp webhook = gson.fromJson(payload, WebhookResp.class);
		System.out.println(webhook.getProject().getName());
		// *****for project************/
		if (projectRepository.projectExistsByName(webhook.getProject().getName())) {
			//// get the project if exists
			System.out.print("project existant avec");
			project = projectRepository.findProjectByname((webhook.getProject().getName()));
			System.out.println(project.getName());

		} else {
			project.setName(webhook.getProject().getName());
			project.setDescription(webhook.getProject().getDescription());
			project.setNamespace(webhook.getProject().getNamespace());
			projectRepository.save(project);
		}

		/******* for author **************************/
		System.out.println(webhook.getProject().getNamespace());
		for (int i = 0; i < webhook.sizeCommits(); i++) {
			System.out.println(webhook.getCommits(i).getMessage());
			System.out.println(webhook.getCommits(i).getAuthor().getName());

			if (authorRepository.authorExistsByEmail(webhook.getCommits(i).getAuthor().getEmail())) {
				System.out.println("hello author amready esistx eddddd");
				author = authorRepository.findAuthorByEmail(webhook.getCommits(i).getAuthor().getEmail());
				
				System.out.print("author existant avec");
				System.out.println(author.getIdAuthor());
			} else {

				author.setEmail(webhook.getCommits(i).getAuthor().getEmail());
				author.setName(webhook.getCommits(i).getAuthor().getName());
				authorRepository.save(author);

			}
			if (authorProjectRepository.findViolationContraints(author.getIdAuthor(), project.getIdProject()) == null) {
				System.out.print("authorProjectRepository existant avec");

				authorProjectPK.setIdAuthor(author.getIdAuthor());
				authorProjectPK.setIdProject(project.getIdProject());
				authorProject.setAuthorProjectPK(authorProjectPK);
				authorProjectRepository.save(authorProject);
			}

			System.out.println("************************************");
			System.out.println(project.getName());

			/* setting the commit attributes */
			System.out.println("0000000000000000000000000");
			commit.setMessage(webhook.getCommits(i).getMessage());
			commit.setName(webhook.getCommits(i).getName());
			commit.setTimestamp(webhook.getCommits(i).getTimestamp());
			commit.setUrl(webhook.getCommits(i).getUrl());
			commit.setAuthor(author);

			/*** extracting the message from the task *********/
			String[] currencies = webhook.getCommits(i).getMessage().split(" ");

			/***
			 * linking the commit with the task based on the commit message informations
			 *****/
			if (currencies[0].toLowerCase().equals("start")) {
				System.out.print(currencies[0]);
				taskStart = true;
			} else if (currencies[0].toLowerCase().equals("end"))
				taskEnd = true;
			/*** if the task with the key in the commit exixsts,add the task to commit*//////
			System.out.println("zzzzzzaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaazzaaaaaa");
			if(currencies.length>1)
			{if (taskRepository.findTaskByKey(currencies[1]) != null) {
				System.out.println("blablablablablablablablabla");
				Task t = taskRepository.findTaskByKey(currencies[1]);
				t.setStartTask(taskStart);
				t.setEndTask(taskEnd);
				taskRepository.save(t);
				commit.setTask(t);
				
			}}

//				for (String a : currencies) {
//					System.out.println("//**//**//**//**//**//**//");
//	              if(a.toUpperCase().equals("start"))
//			{
//		
//			}
//					System.out.println(a);
//				}
			// author.getCommits().add(commit);
			commitRepository.save(commit);

			System.out.println(commit.getAuthor().getName());

		}
		/*****************************
		 * End get data from Webhook
		 *****************************************/
		response.sendRedirect("/notify");
	}
	@Autowired 
    private SimpMessagingTemplate template;
	@GetMapping(value = "timeTraking")
	public void time() {
		TimeScheduling t = new TimeScheduling();
		//t.demoServiceMethod(template);
	}
//end of the RestApi

}
