package com.tipigit.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tipigit.core.DAO.ProjectRepository;
import com.tipigit.core.entities.Author;
import com.tipigit.core.entities.Project;

@RestController
@CrossOrigin("*")
public class ProjectController {
	@Autowired
	ProjectRepository projectRepository;
	@GetMapping(value ="/ListProjects")
	public List<String> ListProjects() {
		return projectRepository.findProjectName();
	}
@GetMapping(value="/detailProject")
public Project getDetails(@RequestParam Long idProject)
{
	return projectRepository.findProjectById(idProject);}

	
	@PutMapping(value = "/updateProject")
	public String  updateAdmin(@RequestParam Long idProject, @RequestBody Project project) {

		Project currentProject = projectRepository.findProjectById(idProject);

		if (currentProject == null) {
			// return("Unable to update. Admin with id {} not found.");
			return ("null");
		}
currentProject.setDescription(project.getDescription());
currentProject.setName(project.getName());
currentProject.setNamespace(project.getNamespace());
		
	projectRepository.save(currentProject);
		

		return ("success");

	}

	@PostMapping(value = "/addProject")
	public Project addProject(@RequestBody Project p) {
		return projectRepository.save(p);
	}
	@GetMapping(value="/allProject")
	public List<Project> getAllProject()

	{return projectRepository.findAll();
		}
	@GetMapping(value="/countProject")
	public long count()
	{
		return projectRepository.count();
	}
	
}