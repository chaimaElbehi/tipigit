package com.tipigit.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tipigit.core.DAO.AuthorProjectRepository;
import com.tipigit.core.entities.AuthorProject;
import com.tipigit.core.entities.Project;

@RequestMapping("/AuthorByProject")
@RestController
@CrossOrigin("*")
public class AuthorProjectController {
	@Autowired
	AuthorProjectRepository authorProjectRepository;

	@GetMapping(value = "/AuthorsByProjectName")
	public List<String> getAuthorsByProjectName(@RequestParam String nameProject) {
		return authorProjectRepository.findAuthorsByProject(nameProject);
	}

	@GetMapping(value = "/projectByAuthorusername")
	List<Project> findProjectByAuthorUsername(@RequestParam String username) {
		return authorProjectRepository.findProjectByAuthorUsername(username);
	}

	@GetMapping(value = "/projectByAuthorName")
	List<String> findProjectByAuthorName(@RequestParam String authorName) {
		return authorProjectRepository.findProjectByAuthors(authorName);
	}

	@GetMapping(value = "/NumberprojectByAuthorName")
	public List<AuthorProject> findAuthorProject() {

		return authorProjectRepository.findNumberProjectByAuthorName();
	}
}
