package com.tipigit.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tipigit.core.DAO.TeamRepository;
import com.tipigit.core.entities.Author;
import com.tipigit.core.entities.Project;
import com.tipigit.core.entities.Team;

@RestController
@CrossOrigin("*")

public class TeamController {
	@Autowired
	TeamRepository teamRepository;
	@PostMapping(value = "/addTeam")
	public Team addProject(@RequestParam String TeamName) {
		Team t=new Team();
		t.setNameLeader(TeamName);
		return teamRepository.save(t);
		
	}
	@GetMapping(value="/getTeam")
	public Team getTeam(@RequestParam Long idteam)
	{
	return teamRepository.findTeamById(idteam);	
	}
	 @GetMapping(value="/getAllTeam")
	 public List<Team> getAllTeam()
	 {
		 return teamRepository.findAll();
	 }
	
}
