package com.tipigit.core.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;

public class WebhookResp {
	String object_kind;
	String message;
	String event_name;
	String user_name;
	int total_commits_count;

	List<CommitModel> commits;
	ProjectModel project;
	

	public ProjectModel getProject() {
		return project;
	}

	public void setProject(ProjectModel project) {
		this.project = project;
	}

	public String getObject_kind() {
		return object_kind;
	}

	public void setObject_kind(String object_kind) {
		this.object_kind = object_kind;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEvent_name() {
		return event_name;
	}

	public void setEvent_name(String event_name) {
		this.event_name = event_name;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public int getTotal_commits_count() {
		return total_commits_count;
	}

	public void setTotal_commits_count(int total_commits_count) {
		this.total_commits_count = total_commits_count;
	}

	public int sizeCommits() {
		return commits.size();
	}

	public CommitModel getCommits(int i) {
		return commits.get(i);
	}

	public void setCommits(List<CommitModel> commits) {
		this.commits = commits;
	}

}
