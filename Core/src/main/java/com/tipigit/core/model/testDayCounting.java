package com.tipigit.core.model;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class testDayCounting {
	
	public Date getTaskEndTime(Calendar startDate, int hours){
	    while (hours > 0){
	        int step = 0;
	        if(hours > 24) step = 24;
	        else step = hours;          
	        hours -= step;          
	        startDate.add(Calendar.HOUR_OF_DAY, step);          
	        int dayOfWeek = startDate.get(Calendar.DAY_OF_WEEK);
	        if(dayOfWeek == Calendar.SATURDAY) hours += 24;
	        if(dayOfWeek == Calendar.SUNDAY) hours += 24;
	    }
	    return startDate.getTime();
	}
	
	public long getAllDays(int dayOfWeek, long businessDays) {
	    long result = 0;
	    if (businessDays != 0) {
	        boolean isStartOnWorkday = dayOfWeek < 6;
	        long absBusinessDays = Math.abs(businessDays);

	        if (isStartOnWorkday) {
	            // if negative businessDays: count backwards by shifting weekday
	            int shiftedWorkday = businessDays > 0 ? dayOfWeek : 6 - dayOfWeek;
	            result = absBusinessDays + (absBusinessDays + shiftedWorkday - 1) / 5 * 2;
	        } else { // start on weekend
	            // if negative businessDays: count backwards by shifting weekday
	            int shiftedWeekend = businessDays > 0 ? dayOfWeek : 13 - dayOfWeek;
	            result = absBusinessDays + (absBusinessDays - 1) / 5 * 2 + (7 - shiftedWeekend);
	        }
	    }
	    return result;
	}
	 @GetMapping(value="/countDay")
	 public void countday() throws ParseException {
		 String sdate="2019/4/29";
		 Date date1=new SimpleDateFormat("yyyy/MM/dd").parse(sdate);
	        ZoneId defaultZoneId = ZoneId.systemDefault();

	        Instant instant = date1.toInstant();
	        LocalDate startDate = instant.atZone(defaultZoneId).toLocalDate();


	//LocalDate startDate = LocalDate.of(2019, 04, 26);
	int businessDays = 2;
	LocalDate endDate = startDate.plusDays(getAllDays(startDate.getDayOfWeek().getValue(), businessDays));
	System.out.println(startDate + (businessDays > 0 ? " plus " : " minus ") + Math.abs(businessDays)
	        + " business days: " + endDate);
	



}
	 @GetMapping(value="/countEndTask")
	 public void calcul() throws ParseException
	 {
		 SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
		 Date date = sdf. parse("26-04-2019");
		 Calendar cal = Calendar. getInstance();
		 cal. setTime(date);
			System.out.println(date);
		 Date d=getTaskEndTime(cal,4);
		 System.out.print(d);
	 }
	 
	 
}