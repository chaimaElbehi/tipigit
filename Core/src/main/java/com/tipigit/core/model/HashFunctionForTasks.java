package com.tipigit.core.model;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.Hashing;

public class HashFunctionForTasks {

	public String HashCodeTask(int numb) {

		return (Hashing.sha256().hashString(Integer.toString(numb), StandardCharsets.UTF_8).toString());
	}
}
