package com.tipigit.core.entities;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Level {
	LEVEL1(0),
	LEVEL2(50),
	LEVEL3(100),
	LEVEL4(150),
	LEVEL5(200),
	LEVEL6(250),
	LEVEL7(300),
	LEVEL8(350),
	LEVEL9(400),
	LEVEL10(450),
	LEVEL11(500),
	LEVEL12(550),
	LEVEL13(600),
	LEVEL14(650),
	LEVEL15(700),
	LEVEL16(750),
	LEVEL17(800),
	LEVEL18(850),
	LEVEL19(900),
	LEVEL20(950),
	LEVEL21(1000),
	LEVEL22(1050),
	LEVEL23(1100),
	LEVEL24(1150),
	LEVEL25(1200);
	private float exp;
	
	
	private Level(float exp) {
		this.exp = exp;
	}

	public float getExp() {
		return exp;
	}
	public static Map getExpAndLevel()
	{
		Map<Float, String> mapLevels = new HashMap<>();
		Arrays.asList(Level.values()).forEach(lev -> mapLevels.put(lev.getExp(), lev.name()));
		return mapLevels;
		
	}
	
	
	
	
}
