package com.tipigit.core.entities;



import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity

public class AuthorProject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private AuthorProjectPK authorProjectPK;
	@ManyToOne
	@JoinColumn(name = "idAuthor", referencedColumnName = "idAuthor", insertable = false, updatable = false)
	private Author author;
	@ManyToOne
	@JoinColumn(name = "idProject", referencedColumnName = "idProject", insertable = false, updatable = false)
	private Project project;


	public AuthorProjectPK getAuthorProjectPK() {
		return authorProjectPK;
	}

	public void setAuthorProjectPK(AuthorProjectPK authorProjectPK) {
		this.authorProjectPK = authorProjectPK;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
