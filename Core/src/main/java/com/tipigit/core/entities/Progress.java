package com.tipigit.core.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Data 
@Entity
public class Progress {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idProgress;
	private double experience;
	@Enumerated(EnumType.STRING)
	private Level level;
	@Enumerated(EnumType.STRING)
	Division division;
	
}
