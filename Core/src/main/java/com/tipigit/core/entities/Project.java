package com.tipigit.core.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Project implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long idProject;
	String name;
	String description;
	String namespace;
	@OneToMany(mappedBy = "project")
	@JsonIgnore
	private List<AuthorProject> authorProjects;
	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private List<Sprint> sprints;
	

	public Project() {
	}
}
