package com.tipigit.core.entities;

public enum Division {
	BRONZE(1), SILVER(5), GOLD(10), PLATINIUM(15), DIAMOND(20), CHALLENGER(25);
	private int level;

	private Division(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

}
