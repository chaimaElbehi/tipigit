package com.tipigit.core.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
@Data
@Entity
public class Task implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long idTask;
	String description;
	String authorName;
	int estimationDuration;
	private String startingDate;
	private String keyTask;
	private Boolean StartTask;
	private Boolean EndTask;
	private String endingDate;
	int lefthours;
	@ManyToOne (cascade=CascadeType.ALL)
	@JoinColumn(name="idSprint",referencedColumnName="idSprint")
	@JsonIgnore
	Sprint sprint;
	public Task(String description, String authorName, int estimationDuration) {
		super();
		this.description = description;
		this.authorName = authorName;
		this.estimationDuration = estimationDuration;
		this.EndTask=false;
		this.StartTask=false;
	}
	public Task() {
		super();
	}
	

}
