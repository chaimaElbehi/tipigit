package com.tipigit.core.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data

@Entity
public class Sprint implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idSprint;
	private String sprintName;
	private String startingDate;
	private String endDate;
	private int dayNumber;
	@ManyToOne
	@JoinColumn(name = "idProject", referencedColumnName = "idProject",updatable = true, insertable = true)
	@JsonIgnore
	private Project project;
	@OneToMany(mappedBy = "sprint")
	@JsonIgnore
	private List<Task> tasks;

	public Sprint() {
	}
}
