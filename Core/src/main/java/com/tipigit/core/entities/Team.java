package com.tipigit.core.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Team {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idTeam;
	private String NameLeader;
	private String TeamName;
	@OneToMany(mappedBy="team")
	List<Author> Authors;
	
}
