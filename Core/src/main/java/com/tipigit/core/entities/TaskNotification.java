package com.tipigit.core.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class TaskNotification implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idNotification;
	private String username;
	private boolean state;
	private String message;
	@Column(columnDefinition = "TIMESTAMP")
    private Date dateTimestamp;
	

}
