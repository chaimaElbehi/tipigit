package com.tipigit.core.entities;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Nonnull;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class Author implements Serializable {
	/**
	 * @author ASUS
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idAuthor;
	private String username;
	private String password;
	private String name;
	private String email;
	private String role;
	@OneToMany(mappedBy = "author")
	@JsonIgnore
	private List<AuthorProject> authorProjects;
	@OneToMany(mappedBy = "author")
	@Nonnull
	@JsonIgnore
	private List<Commit> commits;
	@ManyToOne
	@JoinColumn(name = "idTeam", referencedColumnName = "idTeam")
	@JsonIgnore
	Team team;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idProgress", referencedColumnName = "idProgress")
	private Progress progress;

	public Author() {
	}

	public Author(String name, String email, String role) {
		this.name = name;
		this.email = email;
		this.role = role;
	}

}
