package com.tipigit.core.entities;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Commit implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long idCommit;

	String message;

	String timestamp;
	String url;
	String name;
	@ManyToOne
	@JoinColumn(name = "idAuthor", referencedColumnName = "idAuthor", updatable = true, insertable = true)
	@Nonnull
	@JsonIgnore
	Author author;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idtask", nullable = true)

	@JsonIgnore
	private Task task;

	public Commit()
	{}

}
