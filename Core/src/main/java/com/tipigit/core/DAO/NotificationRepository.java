package com.tipigit.core.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.tipigit.core.entities.Notifications;

@Repository
public interface NotificationRepository extends JpaRepository<Notifications, Long> {

}
