package com.tipigit.core.DAO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.tipigit.core.entities.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
	@Query("select a from Author a  where a.name like :nameUser")
	Author findAuthorByName(@Param("nameUser") String nameUser);

	@Query("select a from Author a  where a.idAuthor = :idUser")
	Author findAuthorById(@Param("idUser") Long idUser);

	@Query("select a.name from Author a  ")
	List<String> findAuthorsName();

	@Query("select a from Author a  where a.email like :emailUser")
	Author findAuthorByEmail(@Param("emailUser") String emailUser);

	@Query("SELECT CASE WHEN COUNT(a) > 0 THEN true ELSE false END FROM Author a WHERE a.email = :emailUser")
	boolean authorExistsByEmail(@Param("emailUser") String emailUser);

	@Query("select a from Author a where a.name like :x")
	public Page<Author> search(@Param("x") String mc, Pageable pageRequest);

	@Query("select a from Author a where a.team.idTeam =:idteam")
	public List<Author> getAllAuthorsTeam(@Param("idteam") Long idteam);
	@Query("select a.name from Author a")
	public List <String> getAuthorsName();
	@Query("select username from Author a ")
	public List<String> Getusernames();
	
	@Query("select username from Author a where a.name like :name ")
	public String GetusernameFromAuthorName(@Param("name") String name);
	@Query("select a from Author a where a.username=:username")
	public Author getAuthorByusername(@Param("username") String username);
	@Query("select a from Author a where a.username=:username and a.password=:password")
	public Author authentification(@Param("username") String username ,@Param("password")String password);
	
	@Query("select a.role from Author a where a.username like :username")
	public String getRoleUser(@Param("username") String username);
	
	
}
