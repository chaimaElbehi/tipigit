package com.tipigit.core.DAO;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tipigit.core.entities.Commit;
import com.tipigit.core.model.CommitInfoModel;

@Repository
public interface CommitRepository extends JpaRepository<Commit, Long>{
	@Query("select c from Commit c where c.author.username like :nameUser order by (c.timestamp) ")
    List<Commit> findallCommitsInfoByUserName(@Param("nameUser") String nameUser);
	@Query("select c.message from Commit c where  c.author.username  like :nameUser")
    List<String> findCommitMessageByAuthor(@Param("nameUser") String nameUser);
	@Query ("select c.timestamp,c.task ,a.id,a.name,t.estimationDuration  from Commit c,Task t,Author a  where t.authorName=a.name and t.StartTask=true and c.task=t ")
	List<Object> findInfoNotification();
}
