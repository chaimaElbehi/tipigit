package com.tipigit.core.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tipigit.core.entities.AuthorProject;
import com.tipigit.core.entities.Project;
@Repository
public interface AuthorProjectRepository extends JpaRepository<AuthorProject, Long> {
	@Query("select AP from AuthorProject AP  where AP.author.idAuthor = :idAuthor and AP.project.idProject=:idProject")
	AuthorProject findViolationContraints(@Param("idAuthor") Long idAuthor, @Param("idProject") Long idProject);

	@Query("select AP.author.name from AuthorProject AP where   AP.project.name   like :nameProject")
	List<String> findAuthorsByProject(@Param("nameProject") String nameProject);

	@Query("select AP.project.name from AuthorProject AP where  AP.author.name   like :nameAuthor")
	List<String> findProjectByAuthors(@Param("nameAuthor") String nameAuthor);
	
	@Query("select AP.project from AuthorProject AP where  AP.author.username   like :username")
	List<Project> findProjectByAuthorUsername(@Param("username") String username);

	@Query("select AP.author.name, count(AP.project.name) from AuthorProject AP group by (AP.author.name) ")
	List<AuthorProject> findNumberProjectByAuthorName();

}
