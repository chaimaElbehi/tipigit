package com.tipigit.core.DAO;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tipigit.core.entities.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
@Query("select p from Project p  where  p.name like  :nameRepo")
     Project findProjectByname(@Param("nameRepo") String nameRepo);
@Query("select count(*) from Project")
Integer countNumberProject();
@Query	("select p.name from Project p ")
List<String> findProjectName();

@Query("select p from Project p where p.idProject= :idProject ")
Project findProjectById(@Param("idProject") Long idProject);
@Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM Project p  where  p.name like  :nameRepo")
boolean projectExistsByName(@Param("nameRepo") String nameRepo);


}

