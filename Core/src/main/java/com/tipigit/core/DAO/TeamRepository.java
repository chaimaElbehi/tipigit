package com.tipigit.core.DAO;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.tipigit.core.entities.Team;

public interface TeamRepository extends JpaRepository<Team, Long>{
	
@Query("select t from Team t where t.idTeam = :idteam")
Team findTeamById(@Param("idteam") Long idteam);


}
