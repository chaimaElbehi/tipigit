package com.tipigit.core.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tipigit.core.entities.Sprint;
import com.tipigit.core.entities.Task;

public interface SprintRepository extends JpaRepository<Sprint, Long> {
	@Query("select s from Sprint s  where  s.project.name like  :nameProject")
	List<Sprint> findSprintByProject(@Param("nameProject") String nameProject);
	
	@Query("select s.tasks from Sprint s where s.sprintName like :nameSprint and s.project.name like  :nameProject")
	List<Task> findtaskBySprintAndProjectName(@Param("nameSprint") String nameSprint,@Param("nameProject") String nameProject);
	
	
}
