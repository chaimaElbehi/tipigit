package com.tipigit.core.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import com.tipigit.core.entities.Task;

public interface TaskRepository extends JpaRepository<Task, Long>{
@Query("select t from Task t where t.sprint.idSprint= :idsprint order by t.idTask")
List <Task> ListTaskBySprintId(@Param("idsprint") Long idsprint);
@Query("select t from Task t where t.authorName =:authorName")
List<Task> FindTaskByauthorName(@Param("authorName") String authorName);
@Query("select t from Task t where t.idTask =:idtask")
Task findTaskById(@Param ("idtask") Long idtask);
@Query("select t from Task t where t.keyTask like :key")
Task findTaskByKey(@Param("key") String key);

@Query("select t from Task t,Author a where t.authorName=a.name and a.username like :username and t.StartTask=true")
List<Task>  tasksInProgressByUsername(@Param("username") String username);

@Query("select t from Task t,Author a where t.authorName=a.name and a.username like :username and t.EndTask=true")
List<Task>  tasksDoneByUsername(@Param("username") String username);

@Query("select t from Task t,Author a where t.authorName=a.name and a.username like :username and t.EndTask=false and t.StartTask=false")
List<Task>  taskstoDoByUsername(@Param("username") String username);

@Query("select t.estimationDuration from Task t where t.idTask=:id")
int findEstimationBytaskId(@Param("id") Long id);
@Transactional
@Modifying
@Query("UPDATE Task t set t.lefthours =t.lefthours-1 where t.idTask=:id")
void ReduceLeftHours(@Param("id")Long id);

///*****for the graphe generation ////////////  start but not finished tasks
@Query("select count(*) from Task t  where t.StartTask= true and t.EndTask=false and t.sprint.project.name=:name")
int countNotFinishedTasks(@RequestParam("name") String name);

///*****for the graphe generation ////////////  not start and not finished tasks
@Query("select count(*) from Task t  where t.StartTask= false and t.EndTask=false and t.sprint.project.name=:name")
int countNotStartNotFinishedTasks(@RequestParam("name") String name);
///*****for the grahpe generation ////////////   started and  finished tasks
@Query("select count(*) from Task t  where t.StartTask= true and t.EndTask=true and t.sprint.project.name=:name")
int countFinishedTasks(@RequestParam("name") String name);


//
}

/*@Query("select t from task t where t.sprint.project.authorProjects.author.idAuthor =:idAuthor")
List <Task> findTasksByauthorId(@Param("idAuthor") Long idAuthor);*/

