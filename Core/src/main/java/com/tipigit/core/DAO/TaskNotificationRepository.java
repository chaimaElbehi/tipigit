package com.tipigit.core.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import com.tipigit.core.entities.TaskNotification;
@Repository
public interface TaskNotificationRepository extends JpaRepository<TaskNotification,Long> {
	@Query("select notif from TaskNotification notif where notif.username like :username")
	public List <TaskNotification> GetusernameFromAuthorName(@Param("username") String username);
	@Query("select count(*) from TaskNotification notif where notif.username like :username ")
	Integer countNbrNotificationPerUsername(@Param("username") String username);
	
	
}
