package com.tipigit.core.DTO;

import java.util.List;

import com.tipigit.core.entities.AuthorProject;
import com.tipigit.core.entities.Commit;
import com.tipigit.core.entities.Project;
import com.tipigit.core.entities.Sprint;
import com.tipigit.core.entities.Team;

import lombok.Data;
@Data
public class AuthorDTO {
	private Long idAuthor;
	private String name;
	private String email;
	private String role;
	private List<AuthorProject> authorProjects;
	private List<Commit> commits;
	Team team;

}
