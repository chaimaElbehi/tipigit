package com.tipigit.core.main;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.tipigit.core.entities")
@EnableJpaRepositories(basePackages = "com.tipigit.core.DAO")
@SpringBootApplication
public class ContactsApplication  {

	public static void main(String[] args) {
		SpringApplication.run(ContactsApplication.class, args);
	}

	
}
